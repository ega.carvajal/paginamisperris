$(function() {

    //mensajes error
    $("#errorCorreo").hide();
    $("#errorRut").hide();
    $("#errorNombre").hide();
    $("#errorFecha").hide();
    $("#errorRegion").hide();
    $("#errorCiudad").hide();
    $("#errorVivienda").hide();

    //variables de validacion

    var e_Correo = false;
    var e_Rut = false;
    var e_Nombre = false;
    var e_Fecha = false;
    var e_Region = false;
    var e_Ciudad = false;
    var e_Vivienda = false;

    //Focusout cuando pierde el foco de un elemento
    $("#email").focusout(function() {
        check_Correo();
    });

    $("#rut").focusout(function() {
        check_Rut();
    });

    $("#nombre").focusout(function() {
        check_Nombre();
    });

    $("#fecha").focusout(function() {
        check_Fecha();
    });

    $("#region").focusout(function() {
        check_Region();
    });
    //
    $("region").change(function() {
        check_Region();
    });

    $("#ciudad").focusout(function() {
        check_Ciudad();
    });

    $("#vivienda").focusout(function() {
        check_Vivienda();
    });

    //Codigos

    //Validacion correo, debe incluir @ y .

    function check_Correo() {
        var pattern = new RegExp(/^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i);
        if (pattern.test($("#email").val())) {
            $("#errorCorreo").hide();
        } else {
            $("#errorCorreo").html("Direccion inválida");
            $("#errorCorreo").show();
            e_Correo = true;
        }
    }

    //Validacion RUT chileno

    function check_Rut() {
        var e_Rut = new RegExp(/^([0-9]+-[0-9Kk])$/i);
        if (e_Rut.test($("#rut").val())) {
            $("#errorRut").hide();
        } else {
            $("#errorRut").html("Rut invalido");
            $("#errorRut").show();
            e_Rut = true;
        }
    }

    //Validar nombre, solo sean caracteres letras

    function check_Nombre() {
        var e_Nombre = new RegExp(/^([A-Za-zÑñ\-]{1,})\s([A-Za-zÑñ\-]{1,})$/i);
        if (e_Nombre.test($("#nombre").val())) {
            $("#errorNombre").hide();
        } else {
            $("#errorNombre").html("Debe poner nombre y apellido");
            $("#errorNombre").show();
            e_Nombre = true;
        }
    }

    //Probando validacion nombre, mandado mensaje a la consola
    /*console.log("." + $("#nombre").val() + ".");*/

    //Validacion fecha
    //getFullYear para comparar fechas, el usuario no puede ser menor de edad

    function check_Fecha() {
        var e_Fecha = new Date($("#fecha").val());
        if (e_Fecha.getFullYear() <= 2001) {
            $("#errorFecha").hide();
        } else {
            $("#errorFecha").html("Fecha invalida");
            $("#errorFecha").show();
            e_Fecha = true;
        }
    }

    //Validación región, separación de ciudad con su region correspondiente

    function check_Region() {

        if ($("#region").val() == "") {
            console.log("Campo vacio");
            $("#ciudad").empty();
            var miCombo = document.getElementById("ciudad");
            var miOption0 = document.createElement("option");
            miOption0.text = "Seleccione una ciudad";
            miCombo.add(miOption0);
        }

        console.log($("#region").val());

        if ($("#region").val() == "metropolitana") {
            console.log("Estoy en Santiago");
            $("#ciudad").empty();
            var miCombo = document.getElementById("ciudad");
            var miOption1 = document.createElement("option");
            var miOption2 = document.createElement("option");
            miOption1.text = "Estación Central";
            miOption1.value = "Estación Central";
            miCombo.add(miOption1);

            miOption2.text = "Pedro Aguirre Cerda";
            miOption2.value = "Pedro Aguirre Cerda";
            miCombo.add(miOption2);
        }

        if ($("#region").val() == "tarapaca") {
            console.log("Estoy en el Norte");
            $("#ciudad").empty();
            var miCombo = document.getElementById("ciudad");
            var miOption3 = document.createElement("option");
            var miOption4 = document.createElement("option");
            miOption3.text = "Iquique";
            miOption3.value = "Iquique";
            miCombo.add(miOption3);

            miOption4.text = "Alto Hospicio";
            miOption4.value = "Alto Hospicio";
            miCombo.add(miOption4);
        }
    }

    function check_Ciudad() {

    }

    //Validacion tipo de vivienda

    function check_Vivienda() {
        if (e_Vivienda.test($("#vivienda").val())) {
            $("#errorVivienda").hide();
        } else {
            $("#errorVivienda").html("Por favor seleccione su vivienda");
            $("#errorVivienda").show();
            e_Vivienda = true;
        }
    }

});